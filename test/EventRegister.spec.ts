import { createError } from "./helpers";
import { EventEmitter } from "../source/EventEmiter";
import { createEvent } from "../source/helpers";
import { EventRegister } from "../source/EventRegister";

const evereg = new EventRegister();

if (typeof evereg !== 'object') {
    createError('cTgavK');
}

if (typeof evereg.register !== 'function') {
    createError('QstsqJ');
}

if (typeof evereg.unregister !== 'function') {
    createError('UQLU53');
}

if (typeof evereg.unregisterAll !== 'function') {
    createError('CWJAYJ');
}

const emiter = new EventEmitter();
var count: number = 0;

evereg.register(emiter, 'eventOne', () => {
    count = count + 1;
});

evereg.register(emiter, 'eventOne', () => {
    count = count + 2;
});

evereg.register(emiter, 'eventTwo', () => {
    count = count + 5;
});

emiter.dispatchEvent(createEvent('eventOne'));
emiter.dispatchEvent(createEvent('eventTwo'));

if (count !== 8) {
    createError('Ir4Fq6');
}

evereg.unregister('Two');
emiter.dispatchEvent(createEvent('eventOne'));
emiter.dispatchEvent(createEvent('eventTwo'));

if (count !== 11) {
    createError('KvEEZP');
}

evereg.unregisterAll();
emiter.dispatchEvent(createEvent('eventOne'));
emiter.dispatchEvent(createEvent('eventTwo'));

if (count !== 11) {
    createError('tSjkZS');
}