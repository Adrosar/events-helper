import "./getEventTargetClass.spec";
import "./createEvent.spec";
import "./EventEmiter.spec";
import "./EventRegister.spec";

import { startCheck } from "./helpers";
startCheck();