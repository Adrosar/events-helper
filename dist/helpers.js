"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getDocument() {
    return window.document;
}
exports.getDocument = getDocument;
function getEventTargetClass() {
    const ET = window['EventTarget'];
    if (typeof ET === 'function') {
        try {
            // Fix for Firefox 56
            new ET();
        }
        catch (_) {
            return null;
        }
        return ET;
    }
    return null;
}
exports.getEventTargetClass = getEventTargetClass;
function createEvent(type, data) {
    if (typeof window['Event'] === 'function') {
        const event = new window['Event'](type);
        event.data = data;
        return event;
    }
    else {
        const event = getDocument().createEvent('Event');
        event.initEvent(type, false, true);
        event.data = data;
        return event;
    }
}
exports.createEvent = createEvent;
