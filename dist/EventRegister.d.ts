export declare class EventRegister {
    constructor();
    register(contex: EventTarget, type: string, listener: EventListenerOrEventListenerObject): void;
    unregister(pattern: string): void;
    unregisterAll(): void;
}
