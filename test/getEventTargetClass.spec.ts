import { createError } from "./helpers";
import { getEventTargetClass } from "../source/helpers";

const ET: typeof EventTarget | null = getEventTargetClass();

if (ET === null) {
    // pass
} else {
    if (typeof ET.prototype.addEventListener !== 'function') {
        createError('dP5FRq');
    }

    if (typeof ET.prototype.removeEventListener !== 'function') {
        createError('PhCSHZ');
    }

    if (typeof ET.prototype.dispatchEvent !== 'function') {
        createError('MqQTGH');
    }

    const ETstring: string = '' + ET;

    if (ETstring.indexOf('function') === -1) {
        createError('dUdyba');
    }

    if (ETstring.indexOf('EventTarget') === -1) {
        createError('4pP6uv');
    }
}
