import { createError } from "./helpers";
import { EventEmitter } from "../source/EventEmiter";
import { createEvent } from "../source/helpers";

if (typeof EventEmitter !== 'function') {
    createError('4Xw6Dz');
}

const emiter = new EventEmitter();

if (typeof emiter !== 'object') {
    createError('OXwvcM');
}

if (typeof emiter.addEventListener !== 'function') {
    createError('RNXKIS');
}

if (typeof emiter.removeEventListener !== 'function') {
    createError('rqmze9');
}

if (typeof emiter.dispatchEvent !== 'function') {
    createError('hAHQfW');
}

if (typeof emiter.emitEvent !== 'function') {
    createError('OmSwf8');
}

var index: number = 0;

const onEvent: EventListener = () => {
    index++;
};

const eventTwo: Event = createEvent('event:two');

emiter.addEventListener('event:two', onEvent);
emiter.dispatchEvent(eventTwo);

if (index !== 1) {
    createError('qx3vSt');
}

emiter.removeEventListener('event:two', onEvent);
emiter.dispatchEvent(eventTwo);

if (index !== 1) {
    createError('wtHWdc');
}

emiter.addEventListener('event:one', () => {
    index++;
});

emiter.emitEvent('event:one');

if (index !== 2) {
    createError('vMOK9S');
}
