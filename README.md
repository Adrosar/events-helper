
# Events Helper

## Installation

From  [npmjs.com](https://www.npmjs.com) _(recommended)_ - choose one of the options:

 - `npm install events-helper`
 - `yarn add events-helper`

From [Adrosar/priv-fields](https://bitbucket.org/Adrosar/events-helper) - downloads the entire contents of the repository:
```
npm install bitbucket:Adrosar/events-helper
```

## How to use

### Example 1:

```typescript
import { EventEmitter } from "events-helper";

const target = new EventEmitter();
target.addEventListener('myEventName', (event: Event) => {
    console.log("--->", event);
});

target.emitEvent('myEventName');
```

### Example 2:

```typescript
import { createEvent, EventWithData, EventEmitter  } from "events-helper";

const target = new EventEmitter();

interface MyData {
    boo: string;
    foo: number;
}

target.addEventListener('myEventName', (event: EventWithData<MyData>) => {
    const data: MyData | undefined = event.data;

    if (data) {
        console.log("--->", data.boo);
        console.log("--->", data.foo);
    }
});

target.dispatchEvent(createEvent('myEventName', { boo: 'Boo123', foo: 456 }));
```

## Development

### Running the tests:

For browser:

 1. Run in terminal `npm run rollup:test && npm run browserify:test`
 2. Run server (in terminal) `npm run server`
 3. Open first link [test.browserify.html](http://127.0.0.1:8080/test.browserify.html)
 4. Open second link [test.rollup.html](http://127.0.0.1:8080/test.rollup.html)


## Versioning

I use the versioning system [SemVer](http://semver.org/) _(2.0.0)_

## Author

* **Adrian Gargula**

## License

This project is licensed under the ISC License - see the [wiki/ISC_license](https://en.wikipedia.org/wiki/ISC_license)

## Other

This project is based on [Adrosar/ts-startek-kit](https://bitbucket.org/Adrosar/ts-startek-kit) [_(version 2.0.0)_](https://bitbucket.org/Adrosar/ts-startek-kit/src/2.0.0/)
