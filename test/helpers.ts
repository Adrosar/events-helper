
const errorList: Array<string> = [];
var errorCount: number = 0;

export function createError(msg: string): void {
    errorList.push(msg);
}

function print(...args: Array<any>): void {
    alert(args.join(" "));
}

function done(): void {
    if (errorList.length > 0) {
        print("Errors:", errorList);
    } else {
        print("Test OK :)");
    }
}

function check(): void {
    if (errorList.length > errorCount) {
        errorCount = errorList.length;
        startCheck();
    } else {
        done();
    }
}

export function startCheck(): void {
    window.setTimeout(check, 300);
}