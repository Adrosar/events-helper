import { Private } from "priv-fields";
import { createEvent, getDocument, getEventTargetClass } from "./helpers";

const ET: typeof EventTarget | null = getEventTargetClass();

interface Priv {
    eventTarget: DocumentFragment | EventTarget
}

const priv = new Private<EventEmitter, Priv>();
const __ = priv.createAccess();

function _createET(): DocumentFragment | EventTarget {
    if (ET) {
        return new ET();
    } else {
        return getDocument().createDocumentFragment();
    }
}

export class EventEmitter {

    constructor() {
        priv.initValue(this, {
            eventTarget: _createET()
        });
    }

    public addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void {
        __(this).eventTarget.addEventListener(type, listener, options);
    }

    public removeEventListener(type: string, callback: EventListenerOrEventListenerObject, options?: EventListenerOptions | boolean): void {
        __(this).eventTarget.removeEventListener(type, callback, options);
    }

    public dispatchEvent(event: Event): boolean {
        return __(this).eventTarget.dispatchEvent(event);
    }

    public emitEvent(type: string): void {
        const event: Event = createEvent(type);
        this.dispatchEvent(event);
    }
}