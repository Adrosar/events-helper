export function getDocument(): HTMLDocument {
    return window.document;
}

export function getEventTargetClass(): typeof EventTarget | null {
    const ET: any = (<any>window)['EventTarget'];

    if (typeof ET === 'function') {

        try {
            // Fix for Firefox 56
            new ET();
        } catch (_) {
            return null;
        }

        return ET;
    }

    return null;
}

export interface EventWithData<T> extends Event {
    data?: T;
}

export function createEvent<E extends EventWithData<D>, D>(type: string, data?: D): E {
    if (typeof (<any>window)['Event'] === 'function') {
        const event: E = new (<any>window)['Event'](type);
        event.data = data;
        return event;
    } else {
        const event: E = <E>getDocument().createEvent('Event');
        event.initEvent(type, false, true);
        event.data = data;
        return event;
    }
}