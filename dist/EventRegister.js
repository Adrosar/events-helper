"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const priv_fields_1 = require("priv-fields");
const priv = new priv_fields_1.Private();
const __ = priv.createAccess();
class EventRegister {
    constructor() {
        priv.initValue(this, {
            listenersList: []
        });
    }
    register(contex, type, listener) {
        contex.addEventListener(type, listener);
        __(this).listenersList.push({
            contex: contex,
            type: type,
            listener: listener
        });
    }
    unregister(pattern) {
        const newList = [];
        for (let i = 0; i < __(this).listenersList.length; i++) {
            const item = __(this).listenersList[i];
            if (item.type.indexOf(pattern) > -1) {
                item.contex.removeEventListener(item.type, item.listener);
            }
            else {
                newList.push(item);
            }
        }
        __(this).listenersList = newList;
    }
    unregisterAll() {
        for (let i = 0; i < __(this).listenersList.length; i++) {
            const item = __(this).listenersList[i];
            item.contex.removeEventListener(item.type, item.listener);
        }
        __(this).listenersList = [];
    }
}
exports.EventRegister = EventRegister;
