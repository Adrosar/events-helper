export declare class EventEmitter {
    constructor();
    addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
    removeEventListener(type: string, callback: EventListenerOrEventListenerObject, options?: EventListenerOptions | boolean): void;
    dispatchEvent(event: Event): boolean;
    emitEvent(type: string): void;
}
