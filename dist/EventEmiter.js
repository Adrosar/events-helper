"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const priv_fields_1 = require("priv-fields");
const helpers_1 = require("./helpers");
const ET = helpers_1.getEventTargetClass();
const priv = new priv_fields_1.Private();
const __ = priv.createAccess();
function _createET() {
    if (ET) {
        return new ET();
    }
    else {
        return helpers_1.getDocument().createDocumentFragment();
    }
}
class EventEmitter {
    constructor() {
        priv.initValue(this, {
            eventTarget: _createET()
        });
    }
    addEventListener(type, listener, options) {
        __(this).eventTarget.addEventListener(type, listener, options);
    }
    removeEventListener(type, callback, options) {
        __(this).eventTarget.removeEventListener(type, callback, options);
    }
    dispatchEvent(event) {
        return __(this).eventTarget.dispatchEvent(event);
    }
    emitEvent(type) {
        const event = helpers_1.createEvent(type);
        this.dispatchEvent(event);
    }
}
exports.EventEmitter = EventEmitter;
