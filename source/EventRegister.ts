import { Private } from "priv-fields";

interface RegisteredItem {
    contex: EventTarget;
    type: string;
    listener: EventListenerOrEventListenerObject;
}

interface Priv {
    listenersList: Array<RegisteredItem>;
}

const priv = new Private<EventRegister, Priv>();
const __ = priv.createAccess();

export class EventRegister {

    constructor() {
        priv.initValue(this, {
            listenersList: []
        });
    }

    public register(contex: EventTarget, type: string, listener: EventListenerOrEventListenerObject) {
        contex.addEventListener(type, listener);

        __(this).listenersList.push({
            contex: contex,
            type: type,
            listener: listener
        });
    }

    public unregister(pattern: string): void {
        const newList: Array<RegisteredItem> = [];

        for (let i = 0; i < __(this).listenersList.length; i++) {
            const item: RegisteredItem = __(this).listenersList[i];

            if (item.type.indexOf(pattern) > -1) {
                item.contex.removeEventListener(item.type, item.listener);
            } else {
                newList.push(item);
            }
        }

        __(this).listenersList = newList;
    }

    public unregisterAll(): void {

        for (let i = 0; i < __(this).listenersList.length; i++) {
            const item: RegisteredItem = __(this).listenersList[i];
            item.contex.removeEventListener(item.type, item.listener);
        }

        __(this).listenersList = [];
    }
}