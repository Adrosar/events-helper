export declare function getDocument(): HTMLDocument;
export declare function getEventTargetClass(): typeof EventTarget | null;
export interface EventWithData<T> extends Event {
    data?: T;
}
export declare function createEvent<E extends EventWithData<D>, D>(type: string, data?: D): E;
