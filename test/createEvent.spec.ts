import { createError } from "./helpers";
import { createEvent, EventWithData } from "../source/helpers";

const event: EventWithData<number> = createEvent('event:any', 12345);

if (typeof event !== 'object') {
    createError('HQTFcr');
}

if (event.type !== 'event:any') {
    createError('UUtHgO');
}

if (event.data !== 12345) {
    createError('3rRkqb');
}

const eventString: string = event + "";

if (eventString.indexOf('object') === -1) {
    createError('NKDYbt');
}

if (eventString.indexOf('Event') === -1) {
    createError('G7FaAt');
}